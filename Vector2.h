#pragma once
#include "Resourses.h"
#pragma pack(push, VK_PACK)

#ifndef VK_NAMESPACE

#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }

#endif

VK_NAMESPACE_BEG
template<class _Ty>
class Vector2 {
protected:
public:
	_Ty x, y;
	Vector2();

	Vector2(_Ty x, _Ty y);
	Vector2<_Ty>(const Vector2<_Ty> &other);
	~Vector2() = default;
	
	//	+
	Vector2<_Ty> operator+(Vector2 &other);
	Vector2<_Ty> operator+(_Ty &other);
	Vector2<_Ty> operator+(_Ty &&other);
	//	-
	Vector2<_Ty> operator-(Vector2 &other);
	Vector2<_Ty> operator-(_Ty &other);
	Vector2<_Ty> operator-(_Ty &&other);
	//	*
	Vector2<_Ty> operator*(Vector2 &other);
	Vector2<_Ty> operator*(_Ty &other);
	Vector2<_Ty> operator*(_Ty &&other);
	//	/
	Vector2<_Ty> operator/(Vector2 &other);
	Vector2<_Ty> operator/(_Ty &other);
	Vector2<_Ty> operator/(_Ty &&other);
	//	=
	Vector2<_Ty>& operator=(const Vector2<_Ty> &other);
	Vector2<_Ty>& operator=(const Vector2<_Ty> &&other);
	//	+=
	Vector2<_Ty>& operator+=(const Vector2<_Ty> &other);
	Vector2<_Ty>& operator+=(const Vector2<_Ty> &&other);
	Vector2<_Ty>& operator+=(_Ty &&other);
	//	-=
	Vector2<_Ty>& operator-=(const Vector2<_Ty> &other);
	Vector2<_Ty>& operator-=(const Vector2<_Ty> &&other);
	Vector2<_Ty>& operator-=(_Ty &&other);
	//	*=
	Vector2<_Ty>& operator*=(const Vector2<_Ty> &other);
	Vector2<_Ty>& operator*=(const Vector2<_Ty> &&other);
	Vector2<_Ty>& operator*=(_Ty &&other);
	//	/=
	Vector2<_Ty>& operator/=(const Vector2<_Ty> &other);
	Vector2<_Ty>& operator/=(const Vector2<_Ty> &&other);
	Vector2<_Ty>& operator/=(_Ty &&other);


};


//CONSTRUCTORS
template<class _Ty>
Vector2<_Ty>::Vector2(){}
template<class _Ty>
Vector2<_Ty>::Vector2(const Vector2<_Ty>& other){
	x = other.x;
	y = other.y;
}
template<class _Ty>
Vector2<_Ty>::Vector2(_Ty x, _Ty y) {
	this->x = x;
	this->y = y;
}


//	+
template<class _Ty>
Vector2<_Ty> Vector2<_Ty>::operator+(Vector2 & other){
	return Vector2(other.x + x, other.y + y);
}
template<class _Ty>
Vector2<_Ty> Vector2<_Ty>::operator+(_Ty & other) {
	return Vector2(other + x, other + y);
}
template<class _Ty>
Vector2<_Ty> Vector2<_Ty>::operator+(_Ty && other) {
	return Vector2(other + x, other + y);
}	


//	-
template<class _Ty>
Vector2<_Ty> Vector2<_Ty>::operator-(Vector2 & other) {
	return Vector2(other.x - x, other.y - y);
}
template<class _Ty>
Vector2<_Ty> Vector2<_Ty>::operator-(_Ty & other) {
	return Vector2(other - x, other - y);
}
template<class _Ty>
Vector2<_Ty> Vector2<_Ty>::operator-(_Ty && other) {
	return Vector2(other - x, other - y);
}

//	*
template<class _Ty>
Vector2<_Ty> Vector2<_Ty>::operator*(Vector2 & other) {
	return Vector2(other.x * x, other.y * y);
}
template<class _Ty>
Vector2<_Ty> Vector2<_Ty>::operator*(_Ty & other) {
	return Vector2(other * x, other * y);
}
template<class _Ty>
Vector2<_Ty> Vector2<_Ty>::operator*(_Ty && other) {
	return Vector2(other * x, other * y);
}

//	/
template<class _Ty>
Vector2<_Ty> Vector2<_Ty>::operator/(Vector2 & other) {
	return Vector2(other.x / x, other.y / y);
}
template<class _Ty>
Vector2<_Ty> Vector2<_Ty>::operator/(_Ty & other) {
	return Vector2(other / x, other / y);
}
template<class _Ty>
Vector2<_Ty> Vector2<_Ty>::operator/(_Ty && other) {
	return Vector2(other / x, other / y);
}


//	=
template<class _Ty>
Vector2<_Ty>& Vector2<_Ty>::operator=(const Vector2<_Ty>& other){
	x = other.x;
	y = other.y;
	return *this;
}
template<class _Ty>
Vector2<_Ty>& Vector2<_Ty>::operator=(const Vector2<_Ty>&& other) {
	x = other.x;
	y = other.y;
	return *this;
}


//	+=
template<class _Ty>
Vector2<_Ty>& Vector2<_Ty>::operator+=(const Vector2<_Ty>& other) {
	x += other.x;
	y += other.y;
	return *this;
}
template<class _Ty>
Vector2<_Ty>& Vector2<_Ty>::operator+=(const Vector2<_Ty>&& other) {
	x += other.x;
	y += other.y;
	return *this;
}
template<class _Ty>
Vector2<_Ty>& Vector2<_Ty>::operator+=(_Ty &&other) {
	this->x += other;
	this->y += other;
	return *this;
}


//	-=
template<class _Ty>
Vector2<_Ty>& Vector2<_Ty>::operator-=(const Vector2<_Ty>& other) {
	x -= other.x;
	y -= other.y;
	return *this;
}
template<class _Ty>
Vector2<_Ty>& Vector2<_Ty>::operator-=(const Vector2<_Ty>&& other) {
	x -= other.x;
	y -= other.y;
	return *this;
}
template<class _Ty>
Vector2<_Ty>& Vector2<_Ty>::operator-=(_Ty &&other) {
	this->x -= other;
	this->y -= other;
	return *this;
}

//	*=
template<class _Ty>
Vector2<_Ty>& Vector2<_Ty>::operator*=(const Vector2<_Ty>& other) {
	x *= other.x;
	y *= other.y;
	return *this;
}
template<class _Ty>
Vector2<_Ty>& Vector2<_Ty>::operator*=(const Vector2<_Ty>&& other) {
	x *= other.x;
	y *= other.y;
	return *this;
}
template<class _Ty>
Vector2<_Ty>& Vector2<_Ty>::operator*=(_Ty &&other) {
	this->x *= other;
	this->y *= other;
	return *this;
}

//	/=
template<class _Ty>
Vector2<_Ty>& Vector2<_Ty>::operator/=(const Vector2<_Ty>& other) {
	x /= other.x;
	y /= other.y;
	return *this;
}
template<class _Ty>
Vector2<_Ty>& Vector2<_Ty>::operator/=(const Vector2<_Ty>&& other) {
	x /= other.x;
	y /= other.y;
	return *this;
}
template<class _Ty>
Vector2<_Ty>& Vector2<_Ty>::operator/=(_Ty &&other) {
	this->x /= other;
	this->y /= other;
	return *this;
}








/*WORK WITH POINTERS*/
template<class _Ty>
class Vector2<_Ty*> {
public: 
	_Ty* x, y;
	void Delete();

	Vector2();
	Vector2(const Vector2&)				= delete;
	Vector2& operator=(const Vector2&)	= delete;
	~Vector2()							= default;
};
template<class _Ty>
Vector2<_Ty*>::Vector2() {
	x = nullptr;
	y = nullptr;
}
template<class _Ty>
void Vector2<_Ty*>::Delete() {
	delete x;
	delete y;
}


VK_NAMESPACE_END
#pragma pack(pop)