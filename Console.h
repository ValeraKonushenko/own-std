#pragma once
#include "Vector2.h"
#include <cstdio>
#include <conio.h>
#include <Windows.h>


#pragma pack(push, VK_PACK)

#ifndef VK_NAMESPACE

#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }

#endif

VK_NAMESPACE_BEG
class Console{
public:
	/*READ*/
	template<class T>
	Console&	Read				(T &data) {
		scanf_s("%d", &data);
		return *this;
	}
	Console&	Read				(unsigned &data) {
		scanf_s("%u", &data);
		return *this;
	}
	Console&	Read				(float &data) {
		scanf_s("%f", &data);
		return *this;
	}
	Console&	Read				(double &data) {
		scanf_s("%lf", &data);
		return *this;
	}
	/*READ LINE*/
	template<class T>
	Console&	ReadLine			(T &data) {
		scanf_s("%d", &data);
		printf_s("\n");
		return *this;
	}
	Console&	ReadLine			(unsigned &data) {
		scanf_s("%u", &data);
		printf_s("\n");
		return *this;
	}
	Console&	ReadLine			(float &data) {
		scanf_s("%f", &data);
		printf_s("\n");
		return *this;
	}
	Console&	ReadLine			(double &data) {
		scanf_s("%lf", &data);
		printf_s("\n");
		return *this;
	}
	/*READ KEY*/
	char		ReadKey				() {
		return CC(_getch());
	}
	Console&	ReadKey				(char &key) {
		key = CC(_getch());
		return *this;
	}

	/*WRITE*/
	template<class T>
	Console&	Write				(T data) {
		printf_s("%d", data);
		return *this;
	}
	Console&	Write				(unsigned data) {
		printf_s("%u", data);
		return *this;
	}
	Console&	Write				(float data) {
		printf_s("%f", data);
		return *this;
	}
	Console&	Write				(double data) {
		printf_s("%lf", data);
		return *this;
	}
	Console&	Write				(const char * data) {
		printf_s("%s", data);
		return *this;
	}
	Console&	Write				(char data) {
		printf_s("%c", data);
		return *this;
	}
	Console&	Write				(char *data) {
		printf_s("%s", data);
		return *this;
	}
	Console&	Write				(unsigned char data) {
		printf_s("%c", data);
		return *this;
	}
	template<class T>
	Console&	Write				(T *data) {
		printf_s("%p", data);
		printf_s("\n");
		return *this;
	}
	Console&	Write				() {
		printf_s(" ");
		return *this;
	}
	/*WRITE LINE*/
	template<class T>
	Console&	WriteLine			(T data) {
		printf_s("%d", data);
		printf_s("\n");
		return *this;
	}
	Console&	WriteLine			(unsigned data) {
		printf_s("%u", data);
		printf_s("\n");
		return *this;
	}
	Console&	WriteLine			() {
		printf_s("\n");
		return *this;
	}
	Console&	WriteLine			(float data) {
		printf_s("%f", data);
		printf_s("\n");
		return *this;
	}
	Console&	WriteLine			(double data) {
		printf_s("%lf", data);
		printf_s("\n");
		return *this;
	}
	Console&	WriteLine			(const char * data) {
		printf_s("%s", data);
		printf_s("\n");
		return *this;
	}
	Console&	WriteLine			(char data) {
		printf_s("%c", data);
		printf_s("\n");
		return *this;
	}
	Console&	WriteLine			(char *data) {
		printf_s("%s", data);
		printf_s("\n");
		return *this;
	}
	Console&	WriteLine			(unsigned char data) {
		printf_s("%c", data);
		printf_s("\n");
		return *this;
	}
	template<class T>
	Console&	WriteLine			(T *data) {
		printf_s("%p", data);
		printf_s("\n");
		return *this;
	}



	/*WORK WITH THE STYLE*/
	enum class ConsoleColor {
		Black,
		Blue,
		Green,
		Aqua,
		Red,
		Purple,
		Yellow,
		White,
		Gray,
		LightBlue,
		LightGreen,
		LightAqua,
		LightRed,
		LightPurple,
		LightYellow,
		BrightWhite
	};
	Console&	SetColor			(ConsoleColor bg, ConsoleColor fg) {
		/*
		bit 0 - foreground blue
		bit 1 - foreground green
		bit 2 - foreground red
		bit 3 - foreground intensity
		
		bit 4 - background blue
		bit 5 - background green
		bit 6 - background red
		bit 7 - background intensity
		*/
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),
			static_cast<WORD>(fg) | (static_cast<WORD>(bg) << 4));
		return *this;
	}
	Console&	SetCursorPostion	(SHORT x, SHORT y) {		
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), { x,y });
		return *this;
	}
	Console&	Clear				() {
		system("cls");
		return *this;
	}
	Console&	Clear				(int len){
		for (int i = 0; i < len; i++)
			this->Write(" ");
		return *this;
	}
	Console&	Clear				(int len ,SHORT x, SHORT y) {
		SetCursorPostion(x, y);
		for (int i = 0; i < len; i++)
			this->Write(" ");
		return *this;
	}
	Console&	Clear				(int len, SHORT x, SHORT y, 
										ConsoleColor bg, ConsoleColor fg) {
		SetColor(bg, fg);
		SetCursorPostion(x, y);
		for (int i = 0; i < len; i++)
			this->Write(" ");
		return *this;
	}
	Console&	SetBufferSize		(SHORT x, SHORT y) {
		SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), {x,y});
		return *this;

	}
	SHORT		GetRows				()	{
		CONSOLE_SCREEN_BUFFER_INFO c;
		if (!GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &c))
			return 0;
		return c.dwSize.Y;
	}
	SHORT		GetCols				(){
		CONSOLE_SCREEN_BUFFER_INFO c;
		if (!GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &c))
			return 0;
		return c.dwSize.X;
	}
	SHORT		GetCurrentY			(){
		CONSOLE_SCREEN_BUFFER_INFO c;
		if (!GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &c))
			return 0;
		return c.dwCursorPosition.Y;
	}
	SHORT		GetCurrentX			(){
		CONSOLE_SCREEN_BUFFER_INFO c;
		if (!GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &c))
			return 0;
		return c.dwCursorPosition.X;
	}
	Console&	IsShowCaret			(BOOL show)	{
		CONSOLE_CURSOR_INFO ci;
		ci.bVisible = show;
		SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &ci);
	}
	Console&	SetCaretSize		(unsigned char size) {
		CONSOLE_CURSOR_INFO ci;
		ci.dwSize = size;
		SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &ci);
		return *this;
	}
	Console&	ResetColor			() {
		SetColor(ConsoleColor::Black, ConsoleColor::BrightWhite);
		return *this;
	}
	Console&	SetTitle			(const char * title) {
		SetConsoleTitle(title);
		return *this;
	}
	Console&	GetWndSize			(int &w, int &h){
		HWND hWnd = GetConsoleWindow();
		RECT rc;
		GetClientRect(hWnd, &rc);
		w = rc.right; // ������ ������� �������
		h = rc.bottom;
	}
	Vector2<int> GetScreenBufferSize() {
		CONSOLE_SCREEN_BUFFER_INFO c;
		GetConsoleScreenBufferInfo(
			GetStdHandle(STD_OUTPUT_HANDLE), &c);
		return Vector2<int>(c.dwSize.X, c.dwSize.Y);
	}


	/*GENERAL*/
	Console&	operator=			(const Console&)	= delete;
	Console&	operator=			(const Console&&)	= delete;
				Console				()					= default;
				Console				(const Console&)	= delete;
				Console				(const Console&&)	= delete;
				~Console			()					= default;
};
static typename Console Console;
VK_NAMESPACE_END
#pragma pack(pop)