#pragma once
class Exception {
protected:
	const char * reason;
public:
	Exception(const char *const what) {
		reason = what;
	}
	const char* What() {
		return reason;
	}
	const char* What() const {
		return reason;
	}
	~Exception() {
	}
};