#pragma once
#include "vector2.h"
#include "Resourses.h"
#include "Console.h"
#pragma pack(push, VK_PACK)

#ifndef VK_NAMESPACE

#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }

#endif
VK_NAMESPACE_BEG
#define CW(t) vk::Console.Write(t)
#define CP(x, y) vk::Console.SetCursorPostion(x,y)
using Point = vk::Vector2<float>;
class Poly3
{
private:
	void SetLine(Point &a, Point &b);
public:
	Point  p1;
	Point  p2;
	Point  p3;
	Console::ConsoleColor color;
	virtual void Show();
	void SetPosition(	Point a1, 
						Point a2, 
						Point a3);
	Poly3(	float p1x, float p1y, 
			float p2x, float p2y, 
			float p3x, float p3y, 
		Console::ConsoleColor color = Console::ConsoleColor::White);
	Poly3();
	Poly3(const Poly3&)				= default;
	Poly3& operator=(const Poly3&)	= default;
	~Poly3()						= default;
};

VK_NAMESPACE_END
#pragma pack(pop)