#pragma once
#include <cstdio>
#include "Resourses.h"
#pragma pack(push, VK_PACK)

#ifndef VK_NAMESPACE

#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }

#endif
VK_NAMESPACE_BEG

class IStream{
private:
public:
	template<class T>
	IStream& operator>>(T &data) {
		scanf_s("%d", &data);  
		return *this; 
	}
	IStream& operator>>(unsigned &data) {
		scanf_s("%u",&data); 
		return *this;
	}
	IStream& operator>>(float &data) {
		scanf_s("%f",&data); 
		return *this;
	}
	IStream& operator>>(double &data) {
		scanf_s("%lf",&data); 
		return *this;
	}
	IStream() = default;
};
static IStream cin;
VK_NAMESPACE_END
#pragma pack(pop)