#pragma once
#include <stdio.h>
#define endl "\n"
#include "Resourses.h"
#pragma pack(push, VK_PACK)

#ifndef VK_NAMESPACE

#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }

#endif

VK_NAMESPACE_BEG
class OStream {
private:
public:
	template<class T>
	OStream& operator<<(T &data) {
		printf_s("%d", data);
		return *this; 
	}
	OStream& operator<<(unsigned &data) {
		printf_s("%u", data);
		return *this;
	}
	OStream& operator<<(float &data) {
		printf_s("%.7f", data);
		return *this;
	}
	OStream& operator<<(double &data) {
		printf_s("%.16lf", data);
		return *this;
	}
	OStream& operator<<(const char * data) {
		printf_s("%s", data);
		return *this;
	}
	OStream& operator<<(char data) {
		printf_s("%c", data);
		return *this;
	}
	OStream& operator<<(unsigned char data) {
		printf_s("%c", data);
		return *this;
	}
	OStream() = default;
};
static OStream cout;
VK_NAMESPACE_END
#pragma pack(pop)