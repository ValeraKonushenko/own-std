#pragma once
#include "Console.h"

VK_NAMESPACE_BEG
class Button {
public:
	using Color = _Console::ConsoleColor;
protected:
	int x, y;
	int h, w;
	Color bg, fg, border;
public:
	Button(int x, int y, Color bg, Color fg, Color border);
};
VK_NAMESPACE_END