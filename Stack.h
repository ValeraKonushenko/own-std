#pragma once
#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk {
#define VK_NAMESPACE_END }
#endif // VK_NAMESPACE

VK_NAMESPACE_BEG
template<class _Ty>
class Node {
public:
	Node *p;
	_Ty   data;
	Node(_Ty &data, Node *p = nullptr) :
		p(p), data(data){}
};

template<class _Ty>
class Stack {
private:
	Node<_Ty> *top;
	unsigned int size;
	void Equal(Node<_Ty> *i, const Stack& other) {
		if (i != nullptr)
			Equal(i->p, other);			
		if(i != nullptr)
			Push(i->data);

	}
public:
				Stack(const Stack &other){
		*this = other;
	}
				Stack&		operator=(const Stack& other) {
		Node<_Ty> *i = other.top;
		Equal(i, other);
		return *this;
	}
				Stack	() {
		top = nullptr;
		size = 0;
	}
	bool		IsEmpty	() {
		return (size == 0) ? true : false ; 
	}
	unsigned int Size	() {
		return size;
	}
	_Ty&		Top		() {
		return top->data;
	}
	const _Ty&	Top		()const{
		return top->data;
	}
	Stack&		Push	(_Ty &other) {
		Node<_Ty> *p = new Node<_Ty>(other, top);
		top = p;
		size++;
		return *this;
	}
	Stack&		Push	(_Ty &&other) {
		Node<_Ty> *p = new Node<_Ty>(other, top);
		top = p;
		size++;
		return *this;
	}
	Stack&		Pop		(){
		size--;
		Node<_Ty> *p = top->p;
		delete top;
		top = p;
		return *this;
	}
	Stack&		Clear	() {
		Node<_Ty> *fake = top;
		while (fake != nullptr) {
			fake = top->p;
			delete top;
			top = fake;
		}
		size = 0;
		return *this;
	}
				~Stack	() {
		Clear();
	}
};
VK_NAMESPACE_END