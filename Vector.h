#pragma once
#include "Resourses.h"
#pragma pack(push, VK_PACK)
#include "iterator.h"
#include "Exception.h"
#include <new>
#ifndef VK_NAMESPACE

#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }

#endif


//#define GLOBAL_ALLOC_MODE

//////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~!!DECLARATION!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//////////////////////////////////////////////////////////////////////////
VK_NAMESPACE_BEG
template<class _Ty>
class Vector {
private:
	const float standart_alloc_c = 0.1f;
	_Ty			*arr;
	int			size;
	int			capacity;
	float		alloc_c;
	inline int	NewCap			(int new_size){
		capacity = CI(new_size + new_size * alloc_c);
		return capacity;
	}
public:
				Vector			(){
				SetAllocCoefficient(standart_alloc_c);
				arr = nullptr;
				capacity = size = 0;
			}
				Vector			(float alloc_c){
					this->alloc_c = alloc_c;
					arr = nullptr;
					capacity = size = 0;
				}
				Vector			(int reserve) {
					SetAllocCoefficient(standart_alloc_c);
					Reserve(reserve);
				}
				Vector			(int reserve, float alloc_c) {
					SetAllocCoefficient(alloc_c);
					Reserve(reserve);
				}
				Vector(const Vector<_Ty> &other) {
					this->size = other.size;
					this->capacity = other.capacity;
					this->alloc_c = other.alloc_c;
					arr = new _Ty[capacity];
					memcpy(arr, other.arr, size * sizeof(_Ty));
				}
	Vector&		operator=		(const Vector<_Ty> &other) {
		Clear();
		this->size = other.size;
		this->capacity = other.capacity;
		this->alloc_c = other.alloc_c;
		arr = new _Ty[capacity];
		memcpy(arr, other.arr, size * sizeof(_Ty));
	}
				~Vector			() {
				Clear();
			}
	
	//ACCESS
	const _Ty&	operator[]		(int pos) const {
		if (pos < 0 || pos >= capacity)
			throw Exception("Violation of the access rights");
		return arr[pos];
	}
	const _Ty&	SetAt			(int pos, _Ty& thg) {
		if (pos < 0 || pos >= capacity)
			throw Exception("Violation of the access rights");
		arr[pos] = thg;
		size++;
		return arr[pos];
	}
	const _Ty&	SetAt			(int pos, _Ty&& thg) {
		if (pos < 0 || pos >= capacity)
			throw Exception("Violation of the access rights");
		arr[pos] = thg;
		size++;
		return arr[pos];
	}
	const _Ty&	GetAt			(int pos)const {
		if (pos < 0 || pos >= capacity)
			throw Exception("Violation of the access rights");
		return arr[pos];
	}
	const _Ty*	Data			()const {
		return arr;
	}

	//CAPACITY
	int			Reserve			(int new_cap) {
		Clear();
		NewCap(new_cap);
		try	{
			arr = new _Ty[capacity];
		}
		catch (std::bad_alloc) {
			Clear();
		}
		return 0;
	}
	inline bool	Empty			() {
		return size == 0 ? true: false;
	}
	inline int	Size			() {
		return size;
	}
	inline int	Capacity		() {
		return capacity;
	}
	Vector&		ShrinkToFit		(){
		_Ty * new_arr = new _Ty[size];
		memcpy(new_arr, arr, size * sizeof(_Ty));
		delete[]arr;
		arr = new_arr;
		capacity = size;
		return *this;
	}
	
	//MODIFIRES
	Vector&		Clear			() {
		delete[] arr;
		arr = nullptr;
		size = capacity = 0;
		return *this;
	}
	Vector&		SetAllocCoefficient(float new_c) {
		alloc_c = new_c < 1? 1: new_c;
		return *this;
	}
	float		GetAllocCoefficient() {
		return alloc_c;
	}
	Vector&		Insert			(int pos, _Ty&thg) {
		if (pos < 0 || pos >= capacity)
			throw Exception("Violation of the access rights");
		if (size + 1 > size)
			ReSize(size + 1);

		for (int i = size; i > pos; i--)
			arr[i] = arr[i - 1];
		arr[pos] = thg;
		size++;
		return *this;
	}
	Vector&		Insert			(int pos, _Ty&&thg) {
		if (pos < 0 || pos >= size)
			throw Exception("Violation of the access rights");
		if (size + 1 > capacity)
			ReSize(size + 1);

		for (int i = size; i > pos; i--)
			arr[i] = arr[i - 1];
		arr[pos] = thg;
		size++;
		
		return *this;
	}
	int			Emplace(int pos, _Ty &thg) {
		if (pos < 0 || pos >= capacity)
			throw Exception("Violation of the access rights");
		arr[pos] = thg;
	}
	int			Emplace(int pos, _Ty &&thg) {
		if (pos < 0 || pos >= capacity)
			throw Exception("Violation of the access rights");
		arr[pos] = thg;
	}
	Vector&		Erase			(int pos) {
		if (pos < 0 || pos >= size)
			throw Exception("Violation of the access rights");
		for (int i = pos; i < size; i++)
			arr[i] = arr[i + 1];
		size--;
		return *this;
	}
	Vector&		PushBack		(_Ty& thg) {
		if (size + 1 > capacity)
			ReSize(size + 1);
		arr[size++] = thg;
		return *this;
	}
	Vector&		PushBack		(_Ty&& thg) {
		if (size + 1 > capacity)
			ReSize(size + 1);
		arr[size++] = thg;
		return *this;
	}
	Vector&		PopBack			() {
		if (size <= 0)throw Exception("Violation of access rights");
		size--;
		return *this;
	}
	int			ReSize			(int new_size) {
		if (new_size < 0)throw Exception("New size is not correct");
		if (new_size > capacity) {
			int old_capacity = capacity;
			_Ty *tmpp = new _Ty[NewCap(new_size)];
			memcpy(tmpp, arr, sizeof(_Ty) * old_capacity);
			delete[] arr;
			arr = tmpp;
		}
		else {
			size = new_size;
		}
		return new_size;
	}
};


#pragma pack (pop)
VK_NAMESPACE_END