#pragma once
#include <SFML/Graphics.hpp>
class Animation
{
private:
	using ushort = unsigned short;
	int freq;
	ushort x0, y0;
	ushort amo_step;
	ushort txr_res;
	ushort shift_by_x, shift_by_y;
	
	int last_time;
	short curr_step;
	bool up_or_down;//false = down(--);true = up(++)
public:
	Animation(ushort x0, ushort y0, ushort shift_by_x, ushort shift_by_y, ushort amo_step, ushort txr_res, int freq);
	sf::Sprite & RevUpdate(sf::RenderWindow &wnd, sf::Sprite &spr, int game_time,
		float angle = 0);
	sf::Sprite & Update(sf::RenderWindow &wnd, sf::Sprite &spr, int game_time,
		float angle = 0);
	Animation& operator=(const Animation&)	= default;
	Animation(const Animation&)				= default;
	~Animation()							= default;
};

