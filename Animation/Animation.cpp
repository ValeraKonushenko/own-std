#include "Animation.h"
#include <iostream>
using namespace std;

Animation::Animation(ushort x0, ushort y0, ushort shift_by_x, ushort shift_by_y, ushort amo_step, ushort txr_res, int freq):
	freq(freq),
	x0(x0),
	y0(y0),
	amo_step(amo_step),
	txr_res(txr_res),
	shift_by_x(shift_by_x),
	shift_by_y(shift_by_y),
	last_time(0),
	curr_step(0),
	up_or_down(true)
{}

sf::Sprite & Animation::RevUpdate(sf::RenderWindow & wnd, sf::Sprite & spr, int game_time, float angle){
	float old_angle = spr.getRotation();

	spr.setTextureRect(sf::IntRect(x0 + (shift_by_x * curr_step), y0 + (shift_by_y * curr_step), txr_res, txr_res)	);
	if (angle != 0)spr.setRotation(angle);

	if (game_time - last_time >= freq) {
		last_time = game_time;
		if (up_or_down)			curr_step++;
		else					curr_step--;

		if (curr_step >= amo_step) { up_or_down = false; curr_step--; }
		if (curr_step < 0) { up_or_down = true; curr_step++; }
	}
	wnd.draw(spr);
	//back to the old version
	if (angle != 0)						spr.setRotation(old_angle);
	return spr;
}
sf::Sprite & Animation::Update(sf::RenderWindow & wnd, sf::Sprite & spr, int game_time, float angle) {
	float old_angle = spr.getRotation();

	spr.setTextureRect(sf::IntRect(x0 + (shift_by_x * curr_step), y0 + (shift_by_y * curr_step), txr_res, txr_res));
	if (angle != 0)spr.setRotation(angle);

	if (game_time - last_time >= freq) {
		last_time = game_time;
		curr_step++;
		if (curr_step >= amo_step)curr_step = 0;
	}
	wnd.draw(spr);
	//back to the old version
	if (angle != 0)						spr.setRotation(old_angle);
	return spr;
}