#include "Figures.h"
#include <cmath>

using Point = vk::Vector2<float>;
void vk::Poly3::SetLine(Point &a, Point &b){
	float last_y = 0;
	float denom = b.x - a.x;
	float tmp = a.x * b.y - a.y * b.x;
	if (denom == 0.f) {
		denom = b.y - a.y;
		if (denom == 0)return;
		if(a.y <= b.y)
			for (int i = a.y; i <= b.y; i++) {
				int t = 1.f * (i * (b.x - a.x) + tmp) / denom;
				CP(t, i);
				CW('+');
			}
		else
			for (int i = b.y; i <= a.y; i++) {
				int t = 1.f * (i * (b.x - a.x) + tmp) / denom;
				CP(t, i);
				CW('+');
			}
		return;
	}

	if(a.x <= b.x)
		for (int i = a.x; i <= b.x; i++) {
			int t = 1.f * (i * (b.y - a.y) - tmp) / denom;
			if(fabs(last_y - t) > 1 && i != a.x)
				if(t > last_y)
					for (int j = last_y+1; j < t; j++){
						CP(i, j);
						Console.Write('+');
					}
				else
					for (int j = t; j < last_y; j++) {
						CP(i, j);
						Console.Write('+');
					}
			CP(i, t);
			Console.Write('+');
			last_y = t;
		}
	else
		for (int i = b.x; i <= a.x; i++) {
			int t = 1.f * (i * (b.y - a.y) - tmp) / denom;
			if (fabs(last_y - t) > 1 && i != b.x)
				if(t < last_y)
					for (int j = t; j < last_y; j++) {
						CP(i, j);
						Console.Write('+');
					}
				else
					for (int j = last_y+1; j < t; j++) {
						CP(i, j);
						Console.Write('+');
					}
			CP(i, t);
			Console.Write('+');
			last_y = t;
		}
}

void vk::Poly3::Show(){
	using namespace vk;
	Console.SetColor(Console::ConsoleColor::Black, color);
	SetLine(p1, p2);
	SetLine(p2, p3);
	SetLine(p1, p3);
	Console.SetColor(Console::ConsoleColor::Black, Console::ConsoleColor::White);
}

void vk::Poly3::SetPosition(Point a1, Point a2, Point a3){
	p1 = a1;
	p2 = a2;
	p3 = a3;
}

vk::Poly3::Poly3(	float p1x, float p1y,
					float p2x, float p2y, 
					float p3x, float p3y, 
					Console::ConsoleColor color):
	p1(p1x, p1y), p2(p2x, p2y), p3(p3x, p3y), color(color)
{}

vk::Poly3::Poly3(){
	color = Console::ConsoleColor::White;
}

