#pragma once
#include <iostream>
#define TMPLT	template<typename T>
TMPLT
class T3D {
protected:
	int row, col, deep;
	int allocated;
	T *arr;
public:
	T3D();
	T3D(int row, int col, int deep = 1) noexcept(false);//all values have to >0
	T3D				(T3D &)	= delete;
	T3D&operator=	(T3D &) = delete;
	T*		operator()	(int r, int c, int d = 0) noexcept(false);
	int		GetAlloc	();
	bool	Clear		() noexcept(false);
	int		Create		(int row, int col, int deep = 1) noexcept(false);//delete old array(if have) and create new
	int		GetAmoRow	();
	int		GetAmoColumn();
	int		GetAmoDeep	();
			~T3D	();
};



TMPLT T3D<T>::T3D				() {
	arr = NULL;
	this->row  = -1;
	this->col  = -1;
	this->deep = -1;
	allocated = -1;
}
TMPLT T3D<T>::T3D				(int row, int col, int deep) {
	Create(row, col, deep);
}
TMPLT T*	T3D<T>::operator()		(int r, int c, int d) {
	try
	{
		if (allocated <= 0 || row <= 0 || col <= 0 || deep <= 0)std::exception("Error turned to allocated memory. It possible if memory not was allocated.");
		if (r >= row || r < 0) throw std::exception("Your parammtr r(row) biger than the rows in the allocated memory or smaller than 0");
		if (c >= col || c < 0) throw std::exception("Your parammtr c(collumn) biger than the collumns in the allocated mem oryor smaller than 0");
		if (d >= deep|| d < 0)throw std::exception("Your parammtr d(deep) biger than a deep in the allocated memory or smaller than 0");
		return arr + (col*deep) * r + deep * c + d; 
	}
	catch (const std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}
	return NULL;
}
TMPLT int	T3D<T>::Create			(int row, int col, int deep) {
	try
	{
		if (row <= 0 || col <= 0 || deep <= 0)throw std::exception("Value not can be less or equal 0");
		if (arr != NULL)delete[]arr;
		this->row  = row;
		this->col  = col;
		this->deep = deep;
		allocated = (!row ? 1 : row) * (!col ? 1 : col) * (!deep ? 1 : deep);
		arr = new T[allocated];
		memset(arr, 0, allocated * sizeof(T));
	}
	catch (const std::exception& ex)
	{
		std::cout << ex.what() << std::endl;
		arr = NULL;
		this->row = -1;
		this->col = -1;
		this->deep = -1;
		allocated = -1;
	}
	return allocated;
}
TMPLT bool	T3D<T>::Clear()			{ 
	try {
		if (allocated <= 0)throw std::exception("Allocated memory less or equal 0");
		memset(arr, 0, allocated * sizeof(T)); 
		return 1;
	}
	catch (const std::exception &ex) { std::cout << ex.what() << std::endl; }
	return 0;
}
TMPLT int	T3D<T>::GetAmoRow()		{ return row; }
TMPLT int	T3D<T>::GetAmoColumn()	{ return col; }
TMPLT int	T3D<T>::GetAmoDeep()	{ return deep; }
TMPLT int	T3D<T>::GetAlloc()		{ return allocated; }
TMPLT		T3D<T>::~T3D()		{ if(arr != NULL) delete arr; }