#pragma once
#ifndef VK_NAMESPACE

#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }

#endif
VK_NAMESPACE_BEG
#pragma warning(disable: 4310)
#pragma warning(disable: 4309)
#define VK_PACK 2

typedef unsigned int uint;

#define CI static_cast<int>
#define CC static_cast<char>
#define CD static_cast<double>
#define CF static_cast<float>
#define CS static_cast<short>
VK_NAMESPACE_END