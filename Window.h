#pragma once
#include "Console.h"
#include "Button.h"
VK_NAMESPACE_BEG
class Window {
protected:
	short x, y;
	int w, h;
	_Console::ConsoleColor bg;
	_Console::ConsoleColor border_c;

	void BorderDraw();
public:
	Window					(	
								int x, int y, 
								int w, int h, 
								_Console::ConsoleColor bg,
								_Console::ConsoleColor border
								);
	void			Show	();
	virtual void	Draw	();
	~Window() = default;
};



class Desktop : public Window {
public:
	Desktop(
		int h,
		_Console::ConsoleColor bg = _Console::ConsoleColor::White,
		_Console::ConsoleColor border = _Console::ConsoleColor::BrightWhite);
	~Desktop() = default;
};

class Message : public Window {
private:
	Button bt_ok;
	char * msg;
	int txt_len;
public:
	Message(
		const char *msg,
		int x, int y,
		int w, int h,
		_Console::ConsoleColor bg,
		_Console::ConsoleColor border,
		_Console::ConsoleColor fg
	);
	void Draw()override;
	 ~Message();
};

VK_NAMESPACE_END